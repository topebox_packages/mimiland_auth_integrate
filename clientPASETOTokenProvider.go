package mimiauth

import (
	"encoding/json"
	"errors"

	"aidanwoods.dev/go-paseto"
)

// PasetoMaker is a PASETO token maker
type ClientPASETOTokenProvider struct {
	IClientAuthProvider
	config                PASETOTokenProviderOption
	v4SymmetricKey        paseto.V4SymmetricKey
	v4AsymmetricSecretKey paseto.V4AsymmetricSecretKey
	v4AsymmetricPublicKey paseto.V4AsymmetricPublicKey
}

// NewPasetoMaker creates a new PasetoMaker
func NewClientPASETOTokenProvider(config PASETOTokenProviderOption) (IClientAuthProvider, error) {

	publicAsymmetricHexKey := config.PublicKey

	if len(publicAsymmetricHexKey) != 64 {
		return nil, errors.New("invalid symmetricKey hex key size: must be exactly 64 characters")
	}

	asymmetricPublicKey, errNewPublicKey := paseto.NewV4AsymmetricPublicKeyFromHex(publicAsymmetricHexKey)
	if errNewPublicKey != nil {
		return nil, errors.New("new secret key from hex error, " + errNewPublicKey.Error())
	}

	maker := &ClientPASETOTokenProvider{
		config:                config,
		v4AsymmetricPublicKey: asymmetricPublicKey,
	}

	return maker, nil
}

// VerifyToken checks if the token is valid or not
func (authFeature *ClientPASETOTokenProvider) VerifyToken(token string) (*MimilandPayload, error) {
	payload := &MimilandPayload{}

	parser := paseto.NewParserForValidNow()
	tokenFromSign, errParse := parser.ParseV4Public(authFeature.v4AsymmetricPublicKey, token, nil)
	if errParse != nil {
		return nil, errParse
	}

	if err := json.Unmarshal(tokenFromSign.ClaimsJSON(), &payload); err != nil {
		return nil, err
	}

	return payload, nil
}
