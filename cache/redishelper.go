package mimiauth

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
)

var REDIS_CTX = context.Background()

type ICacheHelper interface {
	IsLogoutSession(ssId string) bool
}

type RedisHelper struct {
	ICacheHelper
	redisClient            *redis.Client
	redisClientForceLogout *redis.Client
}

func (redis *RedisHelper) Init(client *redis.Client, clientForceLogout *redis.Client) {
	redis.redisClient = client
	redis.redisClientForceLogout = clientForceLogout
}

func (redis *RedisHelper) IsLogoutSession(ssId string) bool {
	jsonResult, redisError := redis.redisClientForceLogout.Get(context.Background(), "mimiland-id:force:"+ssId).Result()
	if redisError != nil {
		_ = fmt.Errorf("%v", redisError)
	}
	return len(jsonResult) > 0
}
