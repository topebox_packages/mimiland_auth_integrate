package mimiauth

type PASETOTokenProviderOption struct {
	SymmetricKey                 string
	PublicKey                    string
	PrivateKey                   string
	AccessTokenDurationInMinute  int64
	RefreshTokenDurationInMinute int64
	Issuer                       string
	CurrentShardDb               int32
	ApiKey                       string
	AESDefaultKey                string
	IVKeyDescrypt                string
}
