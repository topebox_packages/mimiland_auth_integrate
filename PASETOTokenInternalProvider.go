package mimiauth

import (
	"encoding/json"
	"fmt"

	"aidanwoods.dev/go-paseto"
)

// PasetoMaker is a PASETO token maker
type PASETOTkInternalProvider struct {
	IClientAuthProvider
	config         PASETOTokenProviderOption
	v4SymmetricKey paseto.V4SymmetricKey
}

// NewPasetoMaker creates a new PasetoMaker
func NewPASETOTokenInternalProvider(config PASETOTokenProviderOption) (*PASETOTkInternalProvider, error) {

	symmetricHexKey := config.SymmetricKey

	if len(symmetricHexKey) != 64 {
		return nil, fmt.Errorf("invalid symmetricKey hex key size: must be exactly %d characters", 64)
	}
	symmetricKey, err := paseto.V4SymmetricKeyFromHex(symmetricHexKey)
	if err != nil {
		return nil, fmt.Errorf(err.Error())
	}
	maker := &PASETOTkInternalProvider{
		config:         config,
		v4SymmetricKey: symmetricKey,
	}

	return maker, nil
}

// VerifyToken checks if the token is valid or not
func (authFeature *PASETOTkInternalProvider) VerifyToken(token string) (*MimilandPayload, error) {
	payload := &MimilandPayload{}

	parser := paseto.NewParserForValidNow()
	tokenFromSign, errParse := parser.ParseV4Local(authFeature.v4SymmetricKey, token, nil)
	if errParse != nil {
		return nil, errParse
	}

	if err := json.Unmarshal(tokenFromSign.ClaimsJSON(), &payload); err != nil {
		return nil, err
	}

	return payload, nil
}
