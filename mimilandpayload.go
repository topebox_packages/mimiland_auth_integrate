package mimiauth

import (
	"errors"
	"time"
)

var (
	ErrInvalidToken = errors.New("token is invalid")
	ErrExpiredToken = errors.New("token has expired")
)

type MimilandPayload struct {
	SessionId  string    `json:"sid"`
	Issuer     string    `json:"iss"`
	UserName   string    `json:"un"`
	IssuedAt   time.Time `json:"iat"`
	ExpiredAt  time.Time `json:"exp"`
	ClientID   string    `json:"aud"` //Audience
	MimiLandID string    `json:"sub"`
	ShardId    int32     `json:"sh"`
	Status     int32     `json:"us"`
	IsAdmin    bool      `json:"ad"`
}

// New Pay load creates a new token payload with a specific username and duration
func NewPayload(issuer string, clientId string, userID string, userName string, duration time.Duration, sessionID string, shardId int32) (*MimilandPayload, error) {

	if issuer == "" {
		issuer = "id.mimiland.com"
	}
	payload := &MimilandPayload{
		Issuer:     issuer,
		SessionId:  sessionID,
		ClientID:   clientId,
		MimiLandID: userID,
		UserName:   userName,
		ShardId:    shardId,
		IssuedAt:   time.Now().UTC(),
		ExpiredAt:  time.Now().UTC().Add(duration),
	}
	return payload, nil
}

func NewRefreshPayload(issuer string, clientId string, userID string, userName string, duration time.Duration, sessionID string) (*MimilandPayload, error) {

	if issuer == "" {
		issuer = "https://id.mimiland.com"
	}
	payload := &MimilandPayload{
		Issuer:     issuer,
		SessionId:  sessionID,
		ClientID:   clientId,
		MimiLandID: userID,
		ShardId:    1,
		IssuedAt:   time.Now(),
		ExpiredAt:  time.Now().Add(duration),
	}
	return payload, nil
}

func (payload *MimilandPayload) Valid() error {
	if time.Now().After(payload.ExpiredAt) {
		return ErrExpiredToken
	}
	return nil
}
