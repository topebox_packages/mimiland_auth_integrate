package mimiauth

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/go-redis/redis/v8"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	cache "gitlab.com/topebox_packages/mimiauth/cache"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	authorizationHeaderKey  = "authorization"
	authorizationTypeBearer = "bearer"
	authorizationPayloadKey = "authorization_payload"
)

type MMCLientAuth struct {
	RedisDbFss           *redis.Client
	PublicPaths          map[string]bool
	DevPaths             map[string]bool
	ApiKeyPaths          map[string]bool
	AllowDevUser         map[string]bool
	ApiKeyInteralPaths   map[string]bool
	PTokenProviderOption PASETOTokenProviderOption
}

//var MMCLientAuth mmCLientAuth

func (a *MMCLientAuth) IsPublicPath(path string) bool {
	if path == "/" || path == "/ping" {
		return true
	}
	return a.PublicPaths[path]
}

func (a *MMCLientAuth) IsAllowDevUser(user string) bool {
	if a.AllowDevUser == nil || len(a.AllowDevUser) == 0 {
		return true
	}
	return a.AllowDevUser[user]
}

func (a *MMCLientAuth) IsDevPath(path string) bool {

	if a.IsPublicPath(path) {
		return false
	}
	if strings.HasPrefix(path, "/v1/dev") {
		return true
	}
	if len(a.DevPaths) == 0 {
		return false
	}
	return a.DevPaths[path]
}
func (a *MMCLientAuth) IsApiKeyPath(path string) bool {

	if a.IsPublicPath(path) {
		return false
	}
	if strings.HasPrefix(path, "/v1/apikey/") {
		return true
	}
	if len(a.ApiKeyPaths) == 0 {
		return false
	}
	return a.ApiKeyPaths[path]
}

func (a *MMCLientAuth) IsApiKeyInternalPath(path string) bool {

	if a.IsPublicPath(path) {
		return true
	}
	if strings.HasPrefix(path, "/v1/internal/") {
		return true
	}
	if len(a.ApiKeyInteralPaths) == 0 {
		return false
	}
	return a.ApiKeyInteralPaths[path]
}

func (a *MMCLientAuth) Init(redis *redis.Client, tokenOpt PASETOTokenProviderOption, publicPaths string) {
	a.RedisDbFss = redis
	a.PTokenProviderOption = tokenOpt
	a.PublicPaths = make(map[string]bool)
	for _, e := range strings.Split(publicPaths, ",") {
		a.PublicPaths[e] = true
	}
}

func (a *MMCLientAuth) Authorize(ctx context.Context) (*MimilandPayload, context.Context, error) {
	token, errGetHeader := grpc_auth.AuthFromMD(ctx, authorizationTypeBearer)
	if errGetHeader != nil {
		return nil, ctx, errGetHeader
	}
	pl, errValidTk := a.ValidateToken(token)
	if errValidTk != nil {
		return nil, ctx, status.Errorf(codes.Code(401), errValidTk.Error())
	}
	return pl, ctx, nil
}

func (a *MMCLientAuth) AuthApiToken(w http.ResponseWriter, r *http.Request) (payload *MimilandPayload, err error) {
	if a.IsPublicPath(r.URL.Path) {
		//fmt.Println("path is pass " + r.URL.Path)
		return
	}
	authorizationHeader := r.Header.Get("authorization")
	if len(authorizationHeader) == 0 {
		err = errors.New("authorization header is not provided")
		return
	}

	fields := strings.Fields(authorizationHeader)
	if len(fields) < 2 {
		err = errors.New("invalid authorization header format")
		return
	}

	authorizationType := strings.ToLower(fields[0])
	if authorizationType != authorizationTypeBearer {
		err = errors.New(fmt.Sprintf("unsupported authorization type %s", authorizationType))
		return
	}
	token := fields[1]
	payload, errValidTk := a.ValidateToken(token)
	if errValidTk != nil {
		err = errors.New(fmt.Sprintf(errValidTk.Error()))
		return
	}

	return
}
func (a *MMCLientAuth) ValidateToken(token string) (payload *MimilandPayload, err error) {
	pasetoTokenProvider, err := NewClientPASETOTokenProvider(a.PTokenProviderOption)
	if err != nil {
		return
	}
	payload, err = pasetoTokenProvider.VerifyToken(token)
	if err != nil {
		return
	}
	if len(token) == 0 {
		return nil, errors.New("invalid token")
	}
	if payload != nil && len(payload.SessionId) > 0 {
		var redisHelper = cache.RedisHelper{}

		redisHelper.Init(nil, a.RedisDbFss)
		isLogout := redisHelper.IsLogoutSession(payload.SessionId)
		if isLogout {
			err = errors.New("isForceSession")
			return
		}
	}
	return
}

func (a *MMCLientAuth) AuthTokenDev(w http.ResponseWriter, r *http.Request) (err error) {
	if a.IsDevPath(r.URL.Path) == false {
		return
	}
	authorizationHeader := r.Header.Get("m-authorization")
	if len(authorizationHeader) == 0 {
		err = fmt.Errorf("authorization header is not provided")
		return
	}

	fields := strings.Fields(authorizationHeader)
	if len(fields) < 2 {
		err = fmt.Errorf("invalid authorization header format")
		return
	}

	authorizationType := strings.ToLower(fields[0])
	if authorizationType != authorizationTypeBearer {
		err = fmt.Errorf("unsupported authorization type %s", authorizationType)
		return
	}

	token := fields[1]
	_, errValidTk := a.ValidateTokenDev(token)
	if errValidTk != nil {
		err = fmt.Errorf("isForceLogout")
		return
	}
	return
}

func (a *MMCLientAuth) ValidateTokenDev(token string) (payload *MimilandPayload, err error) {
	pasetoTokenProvider, err := NewClientPASETOTokenProvider(a.PTokenProviderOption)
	if err != nil {
		return
	}
	payload, err = pasetoTokenProvider.VerifyToken(token)
	if err != nil {
		return
	}
	if payload.IsAdmin == false || a.IsAllowDevUser(payload.UserName) == false {
		err = errors.New("unauthenticated")
		return
	}
	if payload != nil && len(payload.SessionId) > 0 {
		var redisHelper = cache.RedisHelper{}

		redisHelper.Init(nil, a.RedisDbFss)
		isLogout := redisHelper.IsLogoutSession(payload.SessionId)
		if isLogout {
			err = errors.New("isForceLogout")
			return
		}
	}
	return
}

func (a *MMCLientAuth) IsAuthWithApiKey(w http.ResponseWriter, r *http.Request, checkUrl bool) bool {
	if r.RequestURI == "/" || r.RequestURI == "/ping" {
		return false
	}
	if checkUrl && a.IsApiKeyPath(r.URL.Path) == false {
		return false
	}
	authorizationHeader := r.Header.Get("mm-api-key")
	authorizationDevHeaderTk := r.Header.Get("m-authorization")
	authorizationHeaderTk := r.Header.Get("authorization")
	if len(authorizationHeader) > 0 && (len(authorizationDevHeaderTk) == 0 || len(authorizationHeaderTk) == 0) {
		return true
	}
	return false
}

func (a *MMCLientAuth) AuthApiKey(w http.ResponseWriter, r *http.Request) (err error) {
	if r.RequestURI == "/" || r.RequestURI == "/ping" {
		return nil
	}
	if len(a.PTokenProviderOption.ApiKey) == 0 || len(a.PTokenProviderOption.AESDefaultKey) == 0 || len(a.PTokenProviderOption.IVKeyDescrypt) == 0 {
		err = fmt.Errorf("missing key for authentication with api key")
		return
	}
	authorizationHeader := r.Header.Get("mm-api-key")
	authorizationDevHeaderTk := r.Header.Get("m-authorization")
	authorizationHeaderTk := r.Header.Get("authorization")
	if len(authorizationHeader) == 0 && (len(authorizationDevHeaderTk) == 0 && len(authorizationHeaderTk) == 0) {
		err = fmt.Errorf("authorization api key header is not provided")
		return
	}
	if len(authorizationHeader) == 0 {
		return nil
	}
	clientIdHeader := r.Header.Get("mm-client-id")
	md5 := GetMD5Hash(fmt.Sprintf("%s.%s", clientIdHeader, a.PTokenProviderOption.ApiKey))
	headerDescrypt := DecryptString(a.PTokenProviderOption.AESDefaultKey, authorizationHeader, a.PTokenProviderOption.IVKeyDescrypt)

	if strings.ToLower(md5) != strings.ToLower(headerDescrypt) {
		err = errors.New("invalid authorization api key value")
		return
	}
	return
}

func (a *MMCLientAuth) IsAuthWithInternalApiKey(w http.ResponseWriter, r *http.Request, checkUrl bool) bool {
	if r.RequestURI == "/" || r.RequestURI == "/ping" {
		return false
	}
	if checkUrl && a.IsApiKeyInternalPath(r.URL.Path) == false {
		return false
	}
	authApiHeader := r.Header.Get("mm-api-key")
	authInternalHeaderTk := r.Header.Get("mm-sec-key")
	authInternalServiceHeaderTk := r.Header.Get("mm-service")
	authHeaderTk := r.Header.Get("authorization")
	if len(authApiHeader) > 0 && len(authInternalHeaderTk) > 0 && len(authInternalServiceHeaderTk) > 0 && len(authHeaderTk) > 0 {
		return true
	}
	return false
}

func (a *MMCLientAuth) AuthInternalApiKey(w http.ResponseWriter, r *http.Request, checkUrl bool) (err error) {
	if r.RequestURI == "/" || r.RequestURI == "/ping" {
		return nil
	}
	if len(a.PTokenProviderOption.ApiKey) == 0 || len(a.PTokenProviderOption.AESDefaultKey) == 0 || len(a.PTokenProviderOption.IVKeyDescrypt) == 0 {
		err = fmt.Errorf("missing key for authentication with api key")
		return
	}
	if a.IsAuthWithInternalApiKey(w, r, checkUrl) {
		authApiHeader := r.Header.Get("mm-api-key")
		authInternalHeaderTk := r.Header.Get("mm-sec-key")
		authInternalServiceHeaderTk := r.Header.Get("mm-service")
		authHeaderTk := r.Header.Get("authorization")
		clientIdHeader := r.Header.Get("mm-client-id")

		if len(authApiHeader) == 0 {
			err = fmt.Errorf("authorization mm-api-key header is not provided")
			return
		}
		if len(authApiHeader) != 64 {
			err = fmt.Errorf("The length of the api key header is invalid")
			return
		}
		if len(authApiHeader) == 0 {
			err = fmt.Errorf("authorization mm-api-key header is not provided")
			return
		}
		if len(authInternalHeaderTk) == 0 {
			err = fmt.Errorf("authorization mm-sec-key header is not provided")
			return
		}
		if len(authInternalServiceHeaderTk) == 0 {
			err = fmt.Errorf("authorization mm-service header is not provided")
			return
		}

		if len(authHeaderTk) == 0 {
			err = fmt.Errorf("authorization bearer header is not provided")
			return
		}

		md5 := GetMD5Hash(fmt.Sprintf("%s-%s@%s.iam.serviceaccount.mimiland.com", authInternalServiceHeaderTk, authInternalHeaderTk, clientIdHeader))
		headerDescrypt := DecryptString(a.PTokenProviderOption.AESDefaultKey, authApiHeader, a.PTokenProviderOption.IVKeyDescrypt)

		if strings.ToLower(md5) != strings.ToLower(headerDescrypt) {
			err = errors.New("invalid authorization api key value")
			return
		}
	}

	return
}
