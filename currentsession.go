package mimiauth

import "encoding/json"

type SessionContext struct {
	Id             string          `json:"Id,omitempty" bson:"Id,omitempty"`
	UserAgent      string          `json:"UserAgent,omitempty" bson:"UserAgent,omitempty"`
	ClientCode     string          `json:"ClientCode,omitempty" bson:"ClientCode,omitempty"`
	UserId         string          `json:"UserId,omitempty" bson:"UserId,omitempty"`
	UserName       string          `json:"UserName,omitempty" bson:"UserName,omitempty"`
	SessionID      string          `json:"SessionID,omitempty" bson:"SessionID,omitempty"`
	ClientIP       string          `json:"ClientIp,omitempty" bson:"ClientIp,omitempty"`
	AppVersion     string          `json:"AppVersion,omitempty" bson:"AppVersion,omitempty"`
	ClientVersion  string          `json:"ClientVersion,omitempty" bson:"ClientVersion,omitempty"`
	ClientPlatform string          `json:"ClientPlatform,omitempty" bson:"ClientPlatform,omitempty"`
	ClientBundle   string          `json:"ClientBundle,omitempty" bson:"ClientBundle,omitempty"`
	ShardID        int32           `json:"ShardID,omitempty" bson:"ShardID,omitempty"`
	IsAdmin        bool            `json:"IsAdmin,omitempty" bson:"IsAdmin,omitempty"`
	Lang           string          `json:"Lang,omitempty" bson:"Lang,omitempty"`
	Env            string          `json:"Env,omitempty" bson:"Env,omitempty"`
	Region         string          `json:"Region,omitempty" bson:"Region,omitempty"`
	DeviceId       string          `json:"DeviceId,omitempty" bson:"DeviceId,omitempty"`
	DeviceModel    string          `json:"DeviceModel,omitempty" bson:"DeviceModel,omitempty"`
	DeviceSystem   string          `json:"DeviceSystem,omitempty" bson:"DeviceSystem,omitempty"`
	Roles          []string        `json:"Roles,omitempty" bson:"Roles,omitempty"`
	Permits        map[string]bool `json:"Permits,omitempty" bson:"Permits,omitempty"`
}

func (o *SessionContext) MarshalBinary() ([]byte, error) {
	return json.Marshal(o)
}
func (o *SessionContext) UnmarshalBinary(data []byte) error {
	return json.Unmarshal(data, o)
}
