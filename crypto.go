package mimiauth

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strings"
)

func HashSHA256(key string, content string) string {
	mac := hmac.New(sha256.New, []byte(key))
	mac.Write([]byte(content))
	expectedMAC := mac.Sum(nil)
	return fmt.Sprintf("%x", expectedMAC)
}

func EncryptString(key string, IVKey string, content string) string {

	_IV := []byte(IVKey)
	k1 := []byte(key)
	data := []byte(content)
	block, _ := aes.NewCipher(k1)
	stream := cipher.NewCFBEncrypter(block, _IV)
	stream.XORKeyStream(data, data)
	return fmt.Sprintf("%x", data)
}
func DecryptString(key string, content string, ivString string) string {
	var _iv = []byte(ivString)
	bytes, _ := hex.DecodeString(content)
	block, _ := aes.NewCipher([]byte(key))
	stream := cipher.NewCFBDecrypter(block, _iv)

	stream.XORKeyStream(bytes, bytes)

	return fmt.Sprintf("%s", string(bytes))
}

func Base64String(content string) string {
	rawDecodedText := base64.StdEncoding.EncodeToString([]byte(content))
	return rawDecodedText
}

func GetMD5Hash(content string) string {
	hash := md5.Sum([]byte(content))
	return hex.EncodeToString(hash[:])
}

func GetMD5HashUpper(content string) string {
	hash := md5.Sum([]byte(content))
	return strings.ToUpper(hex.EncodeToString(hash[:]))
}

func DecodeBase64String(content string) string {
	rawDecodedText, _ := base64.StdEncoding.DecodeString(content)
	return string(rawDecodedText)
}

// return upper string of InternalServiceApiKey
func HashInternalServiceApiKey(IVKey string, aesKey string, clientId string, clientSerectKey string, service string, oneTimeData string) string {
	var md5 string
	if len(oneTimeData) == 0 {
		md5 = GetMD5HashUpper(fmt.Sprintf("%s-%s@%s.iam.serviceaccount.mimiland.com", service, clientSerectKey, clientId))
	} else {
		md5 = GetMD5HashUpper(fmt.Sprintf("%s-%s-%s@%s.iam.serviceaccount.mimiland.com", service, clientSerectKey, oneTimeData, clientId))
	}
	var encrpytedMd5 = EncryptString(aesKey, IVKey, md5)
	return strings.ToUpper(string(encrpytedMd5))
}
