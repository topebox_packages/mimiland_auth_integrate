# init 
go get "gitlab.com/topebox_packages/mimiauth"

```
import (
    mimiauth "gitlab.com/topebox_packages/mimiauth"
    mimiauthCache "gitlab.com/topebox_packages/mimiauth/cache"
)
```
# Mimi Auth Verify Token.
```
pasetoTokenProvider, err := mimiauth.NewClientPASETOTokenProvider(mimiauth.PASETOTokenProviderOption{ PublicKey: {{PublicKeyHex}},})	
payload, err = pasetoTokenProvider.VerifyToken(token)

payload, errValidTk := mmclientAuth.AuthApiToken(w, r)
```
# Mimi Auth Check Force Logout Token.
```
var redisHelper = mimiauthCache.RedisHelper{}
redisHelper.Init(nil, redisDbFss)
isLogout := redisHelper.IsLogoutSession(payload.SessionId)
```

# Example

```

func BeforeRequest(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				panic(fmt.Errorf("%+v", errors.New(fmt.Sprintf("%v", r))))
			}
		}()
		if allowedOrigin(r.Header.Get("Origin")) {
			corsHeader := AppConfig.Server.CorsHeader
			if len(corsHeader) == 0 {
				corsHeader = "origin, accept, mm-client-id, access-control-allow-methods, content-type, access-control-allow-origin, access-control-allow-credentials, access-control-allow-headers, shard, mm-user-id"
			}
			corsMethod := AppConfig.Server.CorsMethod
			if len(corsMethod) == 0 {
				corsMethod = "GET, HEAD, OPTIONS, POST, PUT"
			}
			cors := AppConfig.Server.Cors
			if len(cors) == 0 {
				cors = "*"
			}
			w.Header().Set("Access-Control-Allow-Origin", cors)
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Methods", corsMethod)
			w.Header().Set("Access-Control-Allow-Headers", corsHeader)
		}
		if r.Method == "OPTIONS" {
			return
		}

		var mmclientAuth mimiauth.MMCLientAuth
		err := container.Resolve(&mmclientAuth)
		if err != nil {
			panic(fmt.Errorf("%+v", errors.New(fmt.Sprintf("%v", err))))
		}
		payload, errValidTk := mmclientAuth.AuthApiToken(w, r)
		if errValidTk == nil || payload == nil {
			h.ServeHTTP(w, r)
			return
		} else if errValidTk != nil || payload == nil {
			data, _ := json.Marshal(&pd.BaseReply{
				Code:    http.StatusUnauthorized,
				Message: errValidTk.Error(),
			})
			contentType := getRequestContentType(r)
			w.Header().Set("Content-Type", contentType)
			_, _ = w.Write(data)
			return
		} else if payload != nil {
			headerClientId := r.Header.Get("mm-client-id")
			vlClientIdHd := validClientIdHeader(headerClientId, payload.ClientID)
			if vlClientIdHd == false {
				data, _ := json.Marshal(&pd.BaseReply{
					Code:    share.CODE_INVALID_CLIENTID_HEADER,
					Message: share.MSG_INVALID_CLIENTID_HEADER,
				})
				contentType := getRequestContentType(r)
				w.Header().Set("Content-Type", contentType)
				_, _ = w.Write(data)
				return
			}
			headerUserId := r.Header.Get("mm-user-id")
			vlUserIdHd := validUserIdHeader(headerUserId, payload.MimiLandID)
			if vlUserIdHd == false {
				data, _ := json.Marshal(&pd.BaseReply{
					Code:    share.CODE_INVALID_USERID_HEADER,
					Message: share.MSG_INVALID_USERID_HEADER,
				})
				contentType := getRequestContentType(r)
				w.Header().Set("Content-Type", contentType)
				_, _ = w.Write(data)
				return
			}
		}
		h.ServeHTTP(w, r)
	})
}

```